package com.tsc.skuschenko.tm.exception.entity.session;

import com.tsc.skuschenko.tm.exception.AbstractException;

public final class AccessForbiddenException extends AbstractException {

    public AccessForbiddenException() {
        super("Error! Access forbidden...");
    }

}
